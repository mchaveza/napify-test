package com.example.knock.snapify.ui.views

interface BaseView {
    fun showLoading()
    fun hideLoading()
}