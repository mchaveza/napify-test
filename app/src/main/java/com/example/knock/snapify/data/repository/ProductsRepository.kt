package com.example.knock.snapify.data.repository

import com.example.knock.snapify.ui.products.models.ProductResponse
import rx.Observable

interface ProductsRepository {
    fun search(query: String): Observable<ProductResponse>
}