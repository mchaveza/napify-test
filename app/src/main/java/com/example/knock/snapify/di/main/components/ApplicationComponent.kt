package com.example.knock.snapify.di.main.components

import com.example.knock.snapify.di.data.DataModule
import com.example.knock.snapify.di.data.DataNetModule
import com.example.knock.snapify.di.domain.DomainModule
import com.example.knock.snapify.di.main.modules.ApplicationModule
import com.example.knock.snapify.ui.base.BaseActivity
import com.example.knock.snapify.ui.base.BaseFragment
import com.example.knock.snapify.ui.home.HomeFragment
import com.example.knock.snapify.ui.location.MapsFragment
import com.example.knock.snapify.ui.products.ProductFragment
import com.example.knock.snapify.ui.shop.ShopFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (DataModule::class), (DataNetModule::class), (DomainModule::class)])
interface ApplicationComponent {
    fun inject(baseActivity: BaseActivity)
    fun inject(baseFragment: BaseFragment)
    fun inject(homeFragment: HomeFragment)
    fun inject(shopFragment: ShopFragment)
    fun inject(productFragment: ProductFragment)
    fun inject(mapsFragment: MapsFragment)
}