package com.example.knock.snapify.di.domain

import com.example.knock.snapify.data.repository.ProductsRepository
import com.example.knock.snapify.di.database.NapifyDatabase
import com.example.knock.snapify.domain.ProductInteractor
import com.example.knock.snapify.domain.ShopInteractor
import dagger.Module
import dagger.Provides

@Module
class DomainModule {

    @Provides
    fun providesShopInteractor(database: NapifyDatabase) =
        ShopInteractor(database)

    @Provides
    fun providesProductsInteractor(repository: ProductsRepository) =
        ProductInteractor(repository)


}