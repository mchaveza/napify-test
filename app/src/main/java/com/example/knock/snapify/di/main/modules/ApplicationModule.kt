package com.example.knock.snapify.di.main.modules

import android.content.Context
import com.example.knock.snapify.NapifyApplication
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: NapifyApplication) {

    @Provides
    @Singleton
    fun providesApplication(): NapifyApplication = application

    @Provides
    @Singleton
    fun providesContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun providesPreferencesHelper(context: Context): SharedPreferencesManager = SharedPreferencesManager(context)

    @Provides
    @Singleton
    fun providesTrackingManager(context: Context): TrackingManager = TrackingManager(context)
}