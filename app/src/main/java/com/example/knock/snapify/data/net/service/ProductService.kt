package com.example.knock.snapify.data.net.service

import com.example.knock.snapify.di.data.DataConfiguration
import com.example.knock.snapify.ui.products.models.ProductResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface ProductService {

    @GET(DataConfiguration.SEARCH)
    fun getProducts(
        @Query("query") query: String,
        @Query("format") format: String = "json",
        @Query("apiKey") apiKey: String = "nt97u5fqm8wduj3ex56gyc62"
    ): Observable<Response<ProductResponse>>

}