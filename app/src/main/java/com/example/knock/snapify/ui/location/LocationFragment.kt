package com.example.knock.snapify.ui.location

import android.Manifest
import com.example.knock.snapify.R
import com.example.knock.snapify.ui.base.BaseFragment
import com.ia.mchaveza.kotlin_library.PermissionManager
import com.ia.mchaveza.kotlin_library.performReplacingTransaction

class LocationFragment : BaseFragment() {

    private val mapsFragment by lazy { MapsFragment() }
    private val permissionFragment by lazy { PermissionFragment() }
    private val permissionManager by lazy { PermissionManager(activity!!, null) }

    override fun getLayout(): Int =
        R.layout.fragment_location

    override fun initView() {
        super.initView()
        checkPermissions()
    }

    private fun setupPermissionFragment() {
        permissionFragment.setFragmentListener(object : PermissionFragment.PermissionFragmentCallback {
            override fun onDisplayMap() {
                displayFragment(true)
            }
        })
    }

    private fun checkPermissions() {
        displayFragment(permissionManager.permissionGranted(Manifest.permission.ACCESS_FINE_LOCATION))
    }

    private fun displayFragment(permissionGranted: Boolean) {
        val selectedFragment = if (permissionGranted) {
            mapsFragment
        } else {
            setupPermissionFragment()
            permissionFragment
        }
        fragmentManager?.performReplacingTransaction(R.id.location_container, selectedFragment, allowStateLoss = true)
    }

}