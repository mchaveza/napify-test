package com.example.knock.snapify.di.data

class DataConfiguration(private val baseUrl: String) {

    fun getBaseUrl(): String = baseUrl

    companion object {
        const val SEARCH = "/v1/search"
    }


}