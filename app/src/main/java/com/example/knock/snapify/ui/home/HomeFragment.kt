package com.example.knock.snapify.ui.home

import android.support.v4.view.ViewPager
import android.view.MenuItem
import com.example.knock.snapify.NapifyApplication
import com.example.knock.snapify.R
import com.example.knock.snapify.ui.base.BaseFragment
import com.example.knock.snapify.ui.home.adapter.HomePagerAdapter
import com.example.knock.snapify.ui.products.ProductFragment
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {

    private var prevMenuItem: MenuItem? = null
    private val productFragment by lazy { ProductFragment() }
    private val menuAdapter by lazy { HomePagerAdapter(fragmentManager!!, productFragment) }

    override fun getLayout(): Int =
        R.layout.fragment_home

    override fun initView() {
        super.initView()
        setupNavigation()
        changeToolbarTitle(getString(R.string.navigation_shop))
    }

    override fun initializeDagger() {
        super.initializeDagger()
        NapifyApplication.getApplicationComponent().inject(this)
    }

    private fun setupNavigation() {
        setupNavigationListener()
        setupPageListener()
        setupViewPager()
    }

    private fun setupNavigationListener() {
        this.home_navigation.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.navigation_shop -> {
                    changeToolbarTitle(getString(R.string.shop_title))
                    this.home_viewpager.currentItem = 0
                    (activity as HomeActivity).showSearchIcon(false)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_products -> {
                    changeToolbarTitle(getString(R.string.product_title))
                    this.home_viewpager.currentItem = 1
                    (activity as HomeActivity).showSearchIcon(true)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_location -> {
                    changeToolbarTitle(getString(R.string.location_title))
                    this.home_viewpager.currentItem = 2
                    (activity as HomeActivity).showSearchIcon(false)
                    return@setOnNavigationItemSelectedListener true
                }
                else -> {
                    return@setOnNavigationItemSelectedListener false
                }
            }
        }
    }

    private fun changeToolbarTitle(title: String) {
        (activity as HomeActivity).changeToolbarTitle(title)
    }

    private fun setupPageListener() {
        this.home_viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                if (prevMenuItem != null) {
                    prevMenuItem?.isChecked = false
                } else {
                    this@HomeFragment.home_navigation.menu.getItem(0).isChecked = false
                }
                this@HomeFragment.home_navigation.menu.getItem(position).isChecked = true
                prevMenuItem = this@HomeFragment.home_navigation.menu.getItem(position)
            }

        })
    }

    private fun setupViewPager() {
        this.home_viewpager.adapter = menuAdapter
        this.home_viewpager.offscreenPageLimit = 2
    }

    fun performSearching(query: String) {
        productFragment.performSearching(query)
    }
}