package com.example.knock.snapify.di.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.knock.snapify.data.dao.ShopDao
import com.example.knock.snapify.data.entity.ShopEntity

@Database(entities = [(ShopEntity::class)], version = 1)
abstract class NapifyDatabase : RoomDatabase() {

    abstract fun shopDao(): ShopDao

}